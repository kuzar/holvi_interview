from decimal import *
from functools import reduce
from itertools import *
from operator import itemgetter

# just show that macro the key might leave a way for future changes.
CATEGORY_NAME_STR = "category"

class Roundup ():

    def __init__(self, input_json):

        lines = input_json.get("invoice_lines", None)
        payment = input_json.get("payments", None)

        lines_price, cate_price = self._processLines(lines)
        payment_price = self._processPrice(payment)

        price_per_category = self._processCategory(cate_price)

        # class Constants
        self.LINE_COUNT = len(lines)
        self.PAYMENT_COUNT = len(payment)
        self.TOTAL_LINE_PRICE = reduce(self._sum, lines_price)
        self.TOTAL_PAYMENT = reduce(self._sum, payment_price)
        self.TOTAL_CATE_COUNT = len(price_per_category)

        """
         we need to normalize the total line price as 1, then match the payment amount to match that.
         for this I have to first know what is the payment/price ratio, and I have to know what is the
         category to price ratio.
         
         
         case one (payless or right amount):
         
         e.g. lines price with cate  10, 30, 60  total 100. 
         payment 10,15,25,30  total 80. pa/pi  = 80/100  paid 80%
         
         category ratio = 10%  30%  60%    
         
         each payment will multiply the category ratio. then will had the result.
        
        """

        import pdb
        pdb.set_trace()

        # Todo get the rate per category to total amount of the money, but devicde them not only by the ratio

        # Todo divide the payment money to portions that match the payment_count with the ratio match each portions.
        # e.g.  12 with 4 payment which have 2 2 4 4

    def _processLines(self,lines):
        lines_price = []
        cate_price = []
        # parsing the lines, the incoming products
        for item in lines:
            price = Decimal(item.get("quantity")) * Decimal(item.get("unit_price_net")) \
                .quantize(Decimal('0.00'), rounding=ROUND_UP)

            lines_price.append(price)
            cate_price.append({
                CATEGORY_NAME_STR: item.get(CATEGORY_NAME_STR),
                "price": price
            })

        return lines_price, cate_price

    def _processPrice(self, payment):

        payment_price = []
        # parsing the incoming payments
        for pm in payment:
            price = Decimal(pm.get("amount")).quantize(
                Decimal('0.00'), rounding=ROUND_UP)
            payment_price.append(price)

        return payment_price

    def _processCategory(self, cate_price):
        price_devide_with_category = []
        # reduce the prices in category
        cate_price = sorted(cate_price, key=itemgetter(CATEGORY_NAME_STR))

        for cate_name, group in groupby(cate_price, key=itemgetter(CATEGORY_NAME_STR)):
            cate_price_arr = []
            for item in group:
                cate_price_arr.append(item.get("price"))

            reduced_price = reduce(self._sum, cate_price_arr)
            price_devide_with_category.append({
                cate_name: reduced_price
            })

        return price_devide_with_category

    def _sum(self, x, y):
        return Decimal(Decimal(x) + Decimal(y)).quantize(Decimal('0.00'), rounding=ROUND_UP)
