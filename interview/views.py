from django.shortcuts import render
from django.views import generic
from django.http import JsonResponse
from .generator import *
from .roundup import *


def indexView(request):

    generator = Generator()
    product_line = generator.generate_random_line()
    return JsonResponse(product_line, status=200, safe=False)


def roundView(request):

    test_json = {
        "invoice_lines": [
            {
                "description": "Holvi T-shirt",
                "quantity": 2,
                "category": "Cloth",
                "unit_price_net": "227.02"
            },
            {
                "description": "Holvi T-shirt",
                "quantity": 1,
                "category": "Cloth",
                "unit_price_net": "7.77"
            },
            {
                "description": "Holvi T-shirt",
                "quantity": 2,
                "category": "Cloth",
                "unit_price_net": "216.02"
            },
            {
                "description": "Holvi Poster",
                "quantity": 10,
                "category": "Posters",
                "unit_price_net": "654.91"
            }
        ],
        "payments": [
            {
                "id": 1,
                "amount": "26.49"
            },
            {
                "id": 2,
                "amount": "565.74"
            }
        ]
    }

    roundup = Roundup(input_json=test_json)

    return JsonResponse({}, status=200, safe=False)
