from random import randint, random
from decimal import *


class Generator ():

    def __init__(self):

        self.PRODUCTS_NAME_ARR = [
            "Holvi T-shirt", "Holvi Poster", "Holvi Mug",
            "Holvi Hoodie", "Holvi Gift Card", "Holvi Pen"
        ]

        self.PRODUCTS_DICT = {
            "Holvi T-shirt": {
                "category": "Cloth"
            },
            "Holvi Poster": {
                "category": "Posters"
            },
            "Holvi Mug": {
                "category": "Misc"
            },
            "Holvi Hoodie": {
                "category": "Cloth"
            },
            "Holvi Gift Card": {
                "category": "Gift Card"
            },
            "Holvi Pen": {
                "category": "Misc"
            }

        }

    def get_product_randomname(self):
        num_point = randint(0, len(self.PRODUCTS_NAME_ARR) - 1)
        return self.PRODUCTS_NAME_ARR[num_point]

    def get_product_property(self, product_name):
        return self.PRODUCTS_DICT.get(product_name, None)

    def get_random_price(self):
        getcontext().prec = 7
        return Decimal(random() * randint(10, 1000)).quantize(Decimal('0.00'), rounding=ROUND_UP)

    def generate_random_line(self):

        prod_name = self.get_product_randomname()
        product_props = self.get_product_property(product_name=prod_name)
        product_catagory = product_props.get("category")
        price = self.get_random_price()

        return {'description': prod_name, 'quantity': randint(1, 10),
                'category': product_catagory, 'unit_price_net': price}

    def generate_invoice(self):

        item_count = randint(1, len(self.PRODUCTS_DICT))
        payment_count = randint(1, len(self.PRODUCTS_DICT))

        items_arr = []
        payment_arr = []

        for i in range(item_count):
            items_arr.append(self.generate_random_line())

        for j in range(payment_count):
            payment_arr.append({
                "id": j+1,
                "amount": self.get_random_price()
            })

        return {
            "invoice_lines": items_arr,
            "payments": payment_arr
        }
